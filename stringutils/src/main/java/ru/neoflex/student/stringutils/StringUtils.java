package ru.neoflex.student.stringutils;

public class StringUtils {

    public static String reverseString(String input) {
        System.out.println(String.format("ENTRY reverseString(input = %s)", input));
        StringBuilder reverseSB = new StringBuilder();
        for (int i = input.length()-1; i >=0 ; i--) {
            reverseSB.append(input.charAt(i));
        }
        String output = reverseSB.toString();
        System.out.println(String.format("EXIT reverseString(output = %s)", output));
        return output;
    }
}
