package ru.neoflex.student.stringutils;

import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Test;

public class StringUtilsTest extends TestCase
{
    @Test
    public void testReverse()
    {
        String real = StringUtils.reverseString("qwerty");
        String actual = "ytrewq";
        Assert.assertEquals(real, actual);
    }
}
