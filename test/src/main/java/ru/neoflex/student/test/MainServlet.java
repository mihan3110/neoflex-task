package ru.neoflex.student.test;

import ru.neoflex.student.stringutils.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String input = req.getParameter("input");
        PrintWriter out = resp.getWriter();
        if (input != null && input.length() > 0) {
            out.print("<h1>Reversed input = " + StringUtils.reverseString(input) + "</h1>");
        } else {
            out.print("<h1>Parameter input is null or empty</h1>");
        }

    }

}
